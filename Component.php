<?php

namespace Repono;

/**
 * Classe que representa um componente.
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 */
class Component
{
	/**
	 * Nome único do componente.
	 * @var	string
	 */
	public $Name;
	
	/**
	 * Título do componente.
	 * @var	string
	 */
	public $Title;
	
	/**
	 * Descrição do componente.
	 * @var	string
	 */
	public $Description;
	
	/**
	 * Versão do componente.
	 * @var	string
	 */
	public $Version;
	
	/**
	 * Lista com os autores do componente.
	 * @var	array
	 */
	public $Authors = array();
	
	/**
	 * Lista com os componentes na qual este depende.
	 * @var	array
	 */
	public $Requires = array();
}