<?php
namespace Repono;

/**
 * Classe para utilização dos componentes,
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 * @abstract
 */
abstract class Repono
{
	/**
	 * 
	 * @param	string	$name
	 * @param	string	$version
	 */
	public static function js($name, $version)
	{
		throw new Exception('Método não implementado');
	}
	
	/**
	 * 
	 * @param	string	$name		
	 * @param	string	$version	
	 */
	public static function css($name, $version)
	{
		throw new Exception('Método não implementado');
	}
	
	/**
	 * 
	 * @param	string	$name		
	 * @param	string	$version	
	 */
	public static function html($name, $version)
	{
		throw new Exception('Método não implementado');
	}
}