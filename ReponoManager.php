<?php

namespace Repono;

/**
 * Classe de gerenciamento dos componentes.
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 */
class ReponoManager
{
	/**
	 * Verifica se há uma versão a ser atualizada de um determinado componente.
	 * @param	string	$name		Nome único do componente.
	 * @param	string	$version	Versão do componente.
	 * @return	
	 */
	public function check($name, $version = 'last')
	{
		throw new Exception('Método não implementado');
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $version
	 * @throws Exception
	 */
	public function download($name, $version = 'last')
	{
		throw new Exception('Método não implementado');
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $version
	 * @throws Exception
	 */
	public function update($name, $version = 'last')
	{
		throw new Exception('Método não implementado');
	}
	
	/**
	 * 
	 * @param type $name
	 * @param type $version
	 * @throws Exception
	 */
	private function connect($name, $version)
	{
		throw new Exception('Método não implementado');
	}
}