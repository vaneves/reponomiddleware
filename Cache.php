<?php

namespace Repono;

/**
 * Classe de gerenciamento do cache dos componentes.
 * 
 * @version		0.1
 * @author		Valdirene da Cruz Neves Júnior <vaneves@vaneves.com>
 * @package		Repono
 * @copyright	(c) 2013, Valdirene da Cruz Neves Júnior
 */
class Cache
{
	/**
	 * Guarda uma instância da própria classe.
	 * @var	Cache
	 */
	protected static $instance = null;
	
	/** Guarda o cache da memória após ler do disco.
	 * @var	array
	 */
	private static $data = array();

	
	/**
	 * Construtor da classe, é privado para ser intanciado somente dentro da própria classe.
	 */
	private function __construct() { }
	
	/**
	 * Retorna uma instância da própria classe.
	 * @return	Cache	Retorna uma instância da própria classe.
	 */
	public static function getInstance()
	{
		if(!self::$instance)
			self::$instance = new self();
		return self::$instance;
	}
	
	/**
	 * Guarda em cache os dados informado como parâmetro
	 * @param	string	$key	Chave onde o cache será guardado. Serve para recuperar os dados do cache.
	 * @param	mixed	$data	Dados a serem gravados no cache.
	 * @param	int		$time	Tempo, em minutos, que os dados ficaram guardados.
	 * @return	boolean			O método returna true, caso consiga guarda o cache, no contráio retorna false.
	 */
	public function write($key, $data, $time = 1)
	{
		$file = array();
		$file['time'] = time() + ($time * 3600);
		$file['data'] = $data;

		self::$data[md5($key)] = $file;

		$status = file_put_contents($this->file($key), serialize($file));
		return $status !== false;
	}
	
	/**
	 * Verifica se existe cache em uma determinada chave.
	 * @param	string	$key	Chave que será verificada.
	 * @return	boolean			Retorna true caso o cache exista, no contrário retorna false.
	 */
	public function has($key)
	{
		return $this->read($key) !== false;
	}
	
	/**
	 * Ler os dados de uma determinada chave.
	 * @param	string	$key	Chave em que os dados serão buscados.
	 * @return	mixed			Retorna os dados caso existam, no contrário retorna null.
	 */
	public function read($key)
	{
		if(isset(self::$data[md5($key)]))
		{
			$file = self::$data[md5($key)];
			if(((int)$file['time']) > time())
				return $file['data'];
		}

		if(file_exists($this->file($key)))
		{
			$file = unserialize(file_get_contents($this->file($key)));
			if(is_array($file))
			{
				if(isset($file['time']) && isset($file['data']) && ((int)$file['time']) > time())
				{
					self::$data[md5($key)] = $file;
					return $file['data'];
				}
			}
		}
		return false;
	}
	
	/**
	 * Remove os dados do cache de uma determinada chave.
	 * @param	string	$key	Chave em que os dados serão removidos.
	 * @return	boolean			Retorna true caso consiga remover o cache, no contrário retorna false.
	 */
	public function delete($key)
	{
		if(file_exists($this->file($key)))
			return unlink($this->file($key));
		return true;
	}
	
	/**
	 * Retorna o endereço do arquivo no disco de acordo com a chave.
	 * @param	string	$key	Chave do cache.
	 * @return	string			Retorna o endereço completo do arquivo do disco.
	 */
	private function file($key)
	{
		return dirname(__FILE__) . '/cache/' . md5($key);
	}
}